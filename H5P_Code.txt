>> H5P Code - Accordion & Find the Words <<

<h3><strong>First Example (Embedded Accordion)</strong></h3>
<iframe src="https://h5p.org/h5p/embed/6724" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *" title="Accordion" width="1090" height="252" frameborder="0" style="width: 100%; height: 251px;"></iframe>
<p><strong><br></strong></p>
<h3><strong>Second Example (Embedded Find the Words)</strong></h3>
<iframe src="https://h5p.org/h5p/embed/554805" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *" title="Find the words" width="1090" height="815" frameborder="0" style="width: 100%; height: 1016px;"></iframe>

<script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>
