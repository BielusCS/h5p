/* Random Operations */


// Aquesta classe simula la lògica que ha d'aplicar-se al codi del component Arithmetic Quiz d'H5P per poder incloure les diverses operacions aritmètiques en un mateix exercici.
class Random_Ops {

    constructor(rOps) {
        // rOps --> array al qual se li assigna l'array d'operacions passat per valor
        this.rOps = rOps;        
        // opsOrder --> array d'operacions ordenades aleatòriament
        this.opsOrder = []
    }

    // Aquest mètode estableix l'ordre d'execució de les operacions
    setRandomOrder() {
        var i, rNum;

        // Es volen executar un total de 10 operacions
        for (i = 0; i < 10; i++) {
            // Es genera un número aleatori entre 0 i 3 (en total hi ha 4 operacions disponibles)
            rNum = Math.floor(Math.random() * 4);
            // rNum s'usa com a índex per seleccionar l'operació a l'array 'rOps'
            // A cada iteració s'afegeix l'operació seleccionada a l'array 'opsOrder'
            this.opsOrder[i] = this.rOps[rNum];
        }
    }

    // Aquest mètode mostra les 10 operacions que s'executarien aleatòriament
    showRandomOrder() {
        var i;
        
        for (i = 0; i < this.opsOrder.length; i++)
            console.log("Operation #" + (i + 1) + " --> " + this.opsOrder[i]);
    }
}

// Es crea una instància de la classe passant com a paràmetre l'array amb les 4 operacions
myROps = new Random_Ops(["suma","resta","multiplición","división"]);
// Es genera l'ordre aleatori d'execució
myROps.setRandomOrder();
// Es mostra l'ordre aleatori resultant
myROps.showRandomOrder();
